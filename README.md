# MultiValEmulator

## What is MultiValEmulator

MultiValEmulator represent an "array" of ValEmulator<T>. It is use to ease
the emulation of memory area like the stack, the heap or sections.

Also, it supports gather and scatter operations with ValEmulator<> as index.

Example:


```
#!c

    // Pseudo code
    ValEmulator<int>        a;
    ValEmulator<int>        b;
    MultiValEmulator        memory(100);

    a = [5, 15];
    b = [-15, 1337];
    memory[a] = b;
    // memory holds a lazy representation of any values it can hold in any case

    etc... (more documentation to come)

```

## Context

MultiValEmulator is developed as part of the Barghest project (barghest.org).
